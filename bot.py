import discord
import asyncio
import datetime
import requests
import json
from random import randint
from apscheduler.schedulers.asyncio import AsyncIOScheduler

sched = AsyncIOScheduler()

print('Connecting...')
client = discord.Client()

with open('config.json') as config:
    config = json.load(config)
    time = str(config["time"])
    token =  str(config["token"])

embed = discord.Embed()

@sched.scheduled_job('cron', day_of_week='mon-sun',  minute=("*/" + time))
async def postImg():
    url = ""
    while (".jpg" not in url and ".png" not in url and ".gif" not in url):
        url = requests.get(config["reddit"][randint(0, len(config["reddit"]))-1] + '/random/.json').json()[0]['data']['children'][0]['data']['url']

    return_img = embed.set_image(url=url)
    for server in list(client.servers):
        await client.send_message(server.default_channel, embed=return_img)


@client.event
async def on_ready():
    print('Ready!')
    sched.start()

client.run(token)
